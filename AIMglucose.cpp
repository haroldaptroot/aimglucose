#include <iostream>
#include <array>
#include <vector>
#include <iomanip>
#include <optional>
#include <utility>
#include <stack>
#include <chrono>
#include <cstdlib>
#include "parallel/MultiSolvers.h"

namespace ArithmeticIdentityMiner
{
    using namespace Glucose;
    using std::vector;
    using std::array;
    using std::optional;
    using std::pair;

    const int bits = 4;

    Lit cnf_and(MultiSolvers& s, Lit a, Lit b)
    {
        Lit v = mkLit(s.newVar());
        s.addClause(a, ~v);
        s.addClause(b, ~v);
        s.addClause(~a, ~b, v);
        return v;
    }

    Lit cnf_or(MultiSolvers& s, Lit a, Lit b)
    {
        Lit v = mkLit(s.newVar());
        s.addClause(~a, v);
        s.addClause(~b, v);
        s.addClause(a, b, ~v);
        return v;
    }

    Lit cnf_or(MultiSolvers& s, Lit a, Lit b, Lit c)
    {
        Lit v = mkLit(s.newVar());
        s.addClause(~a, v);
        s.addClause(~b, v);
        s.addClause(~c, v);
        s.addClause(a, b, c, ~v);
        return v;
    }

    Lit cnf_or(MultiSolvers& s, Lit a, Lit b, Lit c, Lit d)
    {
        Lit v = mkLit(s.newVar());
        s.addClause(~a, v);
        s.addClause(~b, v);
        s.addClause(~c, v);
        s.addClause(~d, v);
        s.addClause(a, b, c, d, ~v);
        return v;
    }

    Lit cnf_xor(MultiSolvers& s, Lit a, Lit b)
    {
        Lit v = mkLit(s.newVar());
        s.addClause(a, b, ~v);
        s.addClause(~a, ~b, ~v);
        s.addClause(~a, b, v);
        s.addClause(a, ~b, v);
        return v;
    }

    Lit cnf_carry(MultiSolvers& s, Lit a, Lit b, Lit c)
    {
        Lit v = mkLit(s.newVar());
        s.addClause(a, b, ~v);
        s.addClause(a, c, ~v);
        s.addClause(b, c, ~v);
        s.addClause(~a, ~b, v);
        s.addClause(~a, ~c, v);
        s.addClause(~b, ~c, v);
        return v;
    }

    void cnf_assert_exactly_one(MultiSolvers& s, Lit* items, int N)
    {
        vector<Lit> cl;
        for (size_t i = 0; i < N; i++)
        {
            cl.push_back(items[i]);
        }
        s.addClause(cl);
        for (size_t i = 0; i < N; i++)
        {
            for (size_t j = 0; j < i; j++)
            {
                s.addClause(~items[i], ~items[j]);
            }
        }
    }

    template<size_t count>
    int sign_extend(int x)
    {
        size_t m = 1;
        m <<= count - 1;
        return (x ^ m) - m;
    }

    template<size_t count>
    bool fits_in(int x)
    {
        size_t m = 1;
        m <<= count - 1;
        return int(-m) <= x && x <= int(m - 1);
    }

    struct InstrV
    {
        array<Lit, 16> OpA;
        array<Lit, 16> OpB;
        Lit isTAdd;
        Lit isTSub;
        Lit isTNeg;
        Lit isWAdd;
        Lit isWSub;
        Lit isWNeg;
        Lit isAnd;
        Lit isXor;
        Lit isOr;
        Lit isNot;
    };

    struct InstrB
    {
        array<bool, 16> OpA;
        array<bool, 16> OpB;
        bool isTAdd;
        bool isTSub;
        bool isTNeg;
        bool isWAdd;
        bool isWSub;
        bool isWNeg;
        bool isAnd;
        bool isXor;
        bool isOr;
        bool isNot;

        bool operator==(const InstrB& other) const
        {
            if (isTAdd != other.isTAdd ||
                isTSub != other.isTSub ||
                isTNeg != other.isTNeg ||
                isWAdd != other.isWAdd ||
                isWSub != other.isWSub ||
                isWNeg != other.isWNeg ||
                isAnd != other.isAnd ||
                isXor != other.isXor ||
                isOr != other.isOr ||
                isNot != other.isNot ||
                getOpA() != other.getOpA() ||
                getOpB() != other.getOpB())
                return false;
            return true;
        }

        int getOpA() const
        {
            for (size_t i = 0; i < 16; i++)
            {
                if (OpA[i])
                    return i;
            }
            return -1;
        }
        
        int getOpB() const
        {
            for (size_t i = 0; i < 16; i++)
            {
                if (OpB[i])
                    return i;
            }
            return -1;
        }

        void print() const
        {
            if (isTAdd + isTSub + isTNeg + isWAdd + isWSub + isWNeg + isAnd + isXor + isOr + isNot != 1)
                throw "bad";
            if (isWAdd)
            {
                std::cout << "addw v" << getOpA() << ", ";
                std::cout << "v" << getOpB() << "\n";
            }
            else if (isWSub)
            {
                std::cout << "subw v" << getOpA() << ", ";
                std::cout << "v" << getOpB() << "\n";
            }
            else if (isWNeg)
            {
                std::cout << "neg v" << getOpA() << "\n";
            }
            else if (isTAdd)
            {
                std::cout << "addt v" << getOpA() << ", ";
                std::cout << "v" << getOpB() << "\n";
            }
            else if (isTSub)
            {
                std::cout << "subt v" << getOpA() << ", ";
                std::cout << "v" << getOpB() << "\n";
            }
            else if (isTNeg)
            {
                std::cout << "tneg v" << getOpA() << "\n";
            }
            else if (isAnd)
            {
                std::cout << "and v" << getOpA() << ", ";
                std::cout << "v" << getOpB() << "\n";
            }
            else if (isXor)
            {
                std::cout << "xor v" << getOpA() << ", ";
                std::cout << "v" << getOpB() << "\n";
            }
            else if (isOr)
            {
                std::cout << "or v" << getOpA() << ", ";
                std::cout << "v" << getOpB() << "\n";
            }
            else if (isNot)
            {
                std::cout << "not v" << getOpB() << "\n";
            }
            else {
                throw "Not implemented";
            }
        }
    };

    class Synthesizer {
        MultiSolvers s;
        Lit True;
        vector<InstrV> progL, progR;
        size_t inputcount;
        vector<vector<array<Lit, bits + 1>>> valuebits;

        bool getModel(Lit l)
        {
            return s.model[var(l)] == l_True;
        }

        array<Lit, bits + 1> gather_operand(vector<array<Lit, bits + 1>>& value, int N, array<Lit, 16>& OP)
        {
            array<Lit, bits + 1> res;
            for (size_t i = 0; i < bits + 1; i++)
            {
                res[i] = mkLit(s.newVar());
                for (size_t j = 0; j < N; j++)
                {
                    s.addClause(~OP[j], value[j][i], ~res[i] );
                    s.addClause(~OP[j], ~value[j][i], res[i]);
                }
            }
            return res;
        }

        void simulate(vector<array<Lit, bits + 1>>& value, vector<InstrV>& prog)
        {
            for (size_t i = inputcount; i < prog.size(); i++)
            {
                Lit useOpA = ~cnf_or(s, prog[i].isWNeg, prog[i].isTNeg, prog[i].isNot);
                array<Lit, bits + 1> OpA = gather_operand(value, i, prog[i].OpA);
                for (size_t j = 0; j < bits + 1; j++)
                {
                    OpA[j] = cnf_and(s, useOpA, OpA[j]);
                }

                array<Lit, bits + 1> OpB = gather_operand(value, i, prog[i].OpB);

                Lit isAnyTrap = cnf_or(s, prog[i].isTAdd, prog[i].isTSub, prog[i].isTNeg);
                Lit isAnyFSub = cnf_or(s, prog[i].isTSub, prog[i].isWSub, prog[i].isWNeg, prog[i].isTNeg);
                Lit isAnyAddSub = cnf_or(s, isAnyFSub, prog[i].isWAdd, prog[i].isTAdd);
                Lit carry = isAnyFSub;

                Lit lastSum;

                for (size_t j = 0; j < bits; j++)
                {
                    Lit OpA_addsub = OpA[j];
                    Lit OpB_addsub = cnf_xor(s, OpB[j], isAnyFSub);
                    Lit addsub = cnf_xor(s, cnf_xor(s, OpA_addsub, OpB_addsub), carry);
                    lastSum = addsub;
                    carry = cnf_carry(s, OpA_addsub, OpB_addsub, carry);

                    s.addClause(~isAnyAddSub, addsub, ~value[i][j]);
                    s.addClause(~isAnyAddSub, ~addsub, value[i][j]);


                    Lit A = OpA[j];
                    Lit B = OpB[j];

                    s.addClause(~prog[i].isAnd, A, ~value[i][j]);
                    s.addClause(~prog[i].isAnd, B, ~value[i][j]);
                    s.addClause(~prog[i].isAnd, ~A, ~B, value[i][j]);
                    s.addClause(~prog[i].isOr, ~A, value[i][j]);
                    s.addClause(~prog[i].isOr, ~B, value[i][j]);
                    s.addClause(~prog[i].isOr, A, B, ~value[i][j]);
                    s.addClause(~prog[i].isXor, A, B, ~value[i][j]);
                    s.addClause(~prog[i].isXor, ~A, ~B, ~value[i][j]);
                    s.addClause(~prog[i].isXor, ~A, B, value[i][j]);
                    s.addClause(~prog[i].isXor, A, ~B, value[i][j]);
                    s.addClause(~prog[i].isNot, B, value[i][j]);
                    s.addClause(~prog[i].isNot, ~B, ~value[i][j]);
                }
                {
                    Lit OpA_addsub = cnf_and(s, useOpA, OpA[bits - 1]);
                    Lit OpB_addsub = cnf_xor(s, OpB[bits - 1], isAnyFSub);
                    Lit addsub = cnf_xor(s, cnf_xor(s, OpA_addsub, OpB_addsub), carry);
                    Lit thisTraps = cnf_or(s, OpA[bits], OpB[bits], cnf_and(s, isAnyTrap, cnf_xor(s, addsub, lastSum)));

                    s.addClause(thisTraps, ~value[i][bits]);
                    s.addClause(~thisTraps, value[i][bits]);
                }
            }
        }

        void construct(size_t lhs_size, size_t rhs_size)
        {
            vector<Lit> leftprogBits, rightprogBits;
            vector<Lit> anyIsTrappingOperation;
            progL.resize(lhs_size);
            progR.resize(rhs_size);
            vector<InstrV>* progg = &progL;
            for (int side = 0; side < 2; side++) {
                vector<InstrV>& prog = *progg;

                for (size_t i = inputcount; i < prog.size(); i++)
                {
                    for (size_t j = 0; j < 16; j++)
                    {
                        prog[i].OpA[j] = mkLit(s.newVar());
                        prog[i].OpB[j] = mkLit(s.newVar());
                    }
                    prog[i].isTAdd = mkLit(s.newVar());
                    prog[i].isTSub = mkLit(s.newVar());
                    prog[i].isTNeg = mkLit(s.newVar());
                    prog[i].isWAdd = mkLit(s.newVar());
                    prog[i].isWSub = mkLit(s.newVar());
                    prog[i].isWNeg = mkLit(s.newVar());
                    prog[i].isAnd = mkLit(s.newVar());
                    prog[i].isXor = mkLit(s.newVar());
                    prog[i].isOr = mkLit(s.newVar());
                    prog[i].isNot = mkLit(s.newVar());

                    s.addClause(~prog[i].isTAdd);
                    s.addClause(~prog[i].isTSub);
                    s.addClause(~prog[i].isTNeg);
                    anyIsTrappingOperation.push_back(prog[i].isTAdd);
                    anyIsTrappingOperation.push_back(prog[i].isTSub);
                    anyIsTrappingOperation.push_back(prog[i].isTNeg);

                    // instr does exactly one operation
                    cnf_assert_exactly_one(s, &prog[i].isTAdd, 10);
                    // for operands A and B, select exactly one specific variable to reference
                    cnf_assert_exactly_one(s, &prog[i].OpA[0], i);
                    cnf_assert_exactly_one(s, &prog[i].OpB[0], i);

                    if (inputcount > 1) {
                        // subtract and logic op cannot have equal operands
                        // prevents "boring" equivalences but with collateral damage
                        for (size_t p = 0; p < i; p++)
                        {
                            s.addClause(~prog[i].isTAdd, ~prog[i].OpA[p], ~prog[i].OpB[p]);
                            s.addClause(~prog[i].isAnd, ~prog[i].OpA[p], ~prog[i].OpB[p]);
                            s.addClause(~prog[i].isOr, ~prog[i].OpA[p], ~prog[i].OpB[p]);
                            s.addClause(~prog[i].isXor, ~prog[i].OpA[p], ~prog[i].OpB[p]);
                            s.addClause(~prog[i].isTSub, ~prog[i].OpA[p], ~prog[i].OpB[p]);
                            s.addClause(~prog[i].isWSub, ~prog[i].OpA[p], ~prog[i].OpB[p]);
                        }
                        // force operands of commutative operations to be ordered
                        // prevents "boring" equivalences but with collateral damage (eg associative property is not detected)
                        for (size_t q = 0; q < i; q++)
                        {
                            for (size_t p = 0; p <= q; p++)
                            {
                                s.addClause(~prog[i].isWAdd, ~prog[i].OpA[q], ~prog[i].OpB[p]);
                                s.addClause(~prog[i].isTAdd, ~prog[i].OpA[q], ~prog[i].OpB[p]);
                                s.addClause(~prog[i].isAnd, ~prog[i].OpA[q], ~prog[i].OpB[p]);
                                s.addClause(~prog[i].isOr, ~prog[i].OpA[q], ~prog[i].OpB[p]);
                                s.addClause(~prog[i].isXor, ~prog[i].OpA[q], ~prog[i].OpB[p]);
                            }
                        }
                    }

                    // gather all the bits in a big bit-string (for the lexicographic constraint at the end)
                    if (progg == &progL)
                    {
                        leftprogBits.insert(leftprogBits.end(), &prog[i].OpA[0], &prog[i].OpA[0] + sizeof(InstrV) / sizeof(Lit));
                    }
                    else
                    {
                        rightprogBits.insert(rightprogBits.end(), &prog[i].OpA[0], &prog[i].OpA[0] + sizeof(InstrV) / sizeof(Lit));
                    }
                }

                vector<Lit> isNeg(prog.size());
                for (size_t i = inputcount; i < prog.size(); i++)
                {
                    Lit isAnyNeg = cnf_or(s, prog[i].isTNeg, prog[i].isWNeg, prog[i].isNot);
                    isNeg[i] = isAnyNeg;
                    s.addClause(~isAnyNeg, prog[i].OpA[0]);
                }

                if (prog.size() > inputcount) {
                    // every input and result is used at least once
                    for (size_t j = 0; j < prog.size() - 1; j++)
                    {
                        vector<Lit> used;
                        for (size_t i = std::max(j + 1, inputcount); i < prog.size(); i++)
                        {
                            used.push_back(prog[i].OpB[j]);
                            used.push_back(cnf_and(s, prog[i].OpA[j], ~isNeg[i]));
                        }
                        s.addClause(used);
                    }
                }

                progg = &progR;
            }

            // there must be at least one trapping operation otherwise we get a plain old normal equivalence
            //if (anyIsTrappingOperation.size() != 0)
            //    s.addClause(anyIsTrappingOperation);

            if (leftprogBits.size() == rightprogBits.size()) {
                // if programs are same size, make lhs lexicographically less
                Lit carry = True;
                for (size_t i = 0; i < leftprogBits.size(); i++)
                {
                    carry = cnf_carry(s, leftprogBits[i], ~rightprogBits[i], carry);
                }
                s.addClause(carry);
            }
        }

        void copyModel(vector<InstrB>& model, vector<InstrV>& prog)
        {
            for (size_t i = inputcount; i < prog.size(); i++)
            {
                for (size_t j = 0; j < 16; j++)
                {
                    model[i].OpA[j] = getModel(prog[i].OpA[j]);
                    model[i].OpB[j] = getModel(prog[i].OpB[j]);
                }
                model[i].isTAdd = getModel(prog[i].isTAdd);
                model[i].isTSub = getModel(prog[i].isTSub);
                model[i].isTNeg = getModel(prog[i].isTNeg);
                model[i].isWAdd = getModel(prog[i].isWAdd);
                model[i].isWSub = getModel(prog[i].isWSub);
                model[i].isWNeg = getModel(prog[i].isWNeg);
                model[i].isAnd = getModel(prog[i].isAnd);
                model[i].isXor = getModel(prog[i].isXor);
                model[i].isOr = getModel(prog[i].isOr);
                model[i].isNot = getModel(prog[i].isNot);
            }
        }

        void applyBan(vector<InstrV>& prog, vector<InstrB>& banned, vector<Lit>& cl)
        {
            for (size_t i = inputcount; i < prog.size(); i++)
            {
                {
                    for (size_t j = 0; j < i; j++)
                    {
                        cl.push_back(banned[i].OpB[j] ? ~prog[i].OpB[j] : prog[i].OpB[j]);
                    }
                }
                // only include operand A if it is used, negations don't use A
                if (!banned[i].isTNeg && !banned[i].isWNeg && !banned[i].isNot)
                {
                    for (size_t j = 0; j < i; j++)
                    {
                        cl.push_back(banned[i].OpA[j] ? ~prog[i].OpA[j] : prog[i].OpA[j]);
                    }
                }
                cl.push_back(banned[i].isTAdd ? ~prog[i].isTAdd : prog[i].isTAdd);
                cl.push_back(banned[i].isTSub ? ~prog[i].isTSub : prog[i].isTSub);
                cl.push_back(banned[i].isTNeg ? ~prog[i].isTNeg : prog[i].isTNeg);
                cl.push_back(banned[i].isWAdd ? ~prog[i].isWAdd : prog[i].isWAdd);
                cl.push_back(banned[i].isWSub ? ~prog[i].isWSub : prog[i].isWSub);
                cl.push_back(banned[i].isWNeg ? ~prog[i].isWNeg : prog[i].isWNeg);
                cl.push_back(banned[i].isAnd ? ~prog[i].isAnd : prog[i].isAnd);
                cl.push_back(banned[i].isXor ? ~prog[i].isXor : prog[i].isXor);
                cl.push_back(banned[i].isOr ? ~prog[i].isOr : prog[i].isOr);
                cl.push_back(banned[i].isNot ? ~prog[i].isNot : prog[i].isNot);
            }
        }

    public:
        Synthesizer(size_t lhs_size, size_t rhs_size, size_t inputcount, vector<vector<int>>& inputs, vector<pair<vector<InstrB>, vector<InstrB>>>& banned)
        {
            lhs_size += inputcount;
            rhs_size += inputcount;
            this->inputcount = inputcount;
            True = mkLit(s.newVar());
            s.addClause(True);
            construct(lhs_size, rhs_size);

            for (auto b : banned)
            {
                vector<Lit> cl;
                applyBan(progL, b.first, cl);
                applyBan(progR, b.second, cl);
                s.addClause(cl);
            }

            vector<Lit> notAllZeroOutput;

            for (auto i : inputs)
            {
                vector<array<Lit, bits + 1>> L(lhs_size);
                vector<array<Lit, bits + 1>> R(rhs_size);
                for (size_t ii = 0; ii < inputcount; ii++)
                {
                    for (size_t j = 0; j < bits; j++)
                    {
                        L[ii][j] = (i[ii] & (1 << j)) ? True : ~True;
                        R[ii][j] = (i[ii] & (1 << j)) ? True : ~True;
                    }
                    L[ii][bits] = ~True;
                    R[ii][bits] = ~True;
                }

                for (size_t ii = inputcount; ii < lhs_size; ii++)
                {
                    for (size_t j = 0; j < bits + 1; j++)
                    {
                        L[ii][j] = mkLit(s.newVar());
                    }
                }
                for (size_t ii = inputcount; ii < rhs_size; ii++)
                {
                    for (size_t j = 0; j < bits + 1; j++)
                    {
                        R[ii][j] = mkLit(s.newVar());
                    }
                }
                valuebits.push_back(L);
                valuebits.push_back(R);
                // force results to be equal
                s.addClause(L[lhs_size - 1][bits], ~R[rhs_size - 1][bits]);
                s.addClause(~L[lhs_size - 1][bits], R[rhs_size - 1][bits]);
                for (size_t j = 0; j < bits; j++)
                {
                    s.addClause(L[lhs_size - 1][bits], R[rhs_size - 1][j], ~L[lhs_size - 1][j]);
                    s.addClause(L[lhs_size - 1][bits], ~R[rhs_size - 1][j], L[lhs_size - 1][j]);
                    //notAllZeroOutput.push_back(L[lhs_size - 1][j]);
                    //notAllZeroOutput.push_back(R[rhs_size - 1][j]);
                }

                simulate(L, progL);
                simulate(R, progR);
            }

            //s.addClause(notAllZeroOutput);
        }

        optional<pair<vector<InstrB>, vector<InstrB>>> synthesize()
        {
            if (s.solve() == l_True)
            {
                vector<InstrB> modelL(progL.size());
                vector<InstrB> modelR(progR.size());
                copyModel(modelL, progL);
                copyModel(modelR, progR);

                return { std::make_pair(modelL, modelR) };
            }
            else {
                return {};
            }
        }
    };

    class Runner {
        array<Lit, bits + 1> gather_operand(vector<array<Lit, bits + 1>>& values, int N, array<bool, 16>& op)
        {
            array<Lit, bits + 1> res;
            int k = -1;
            for (size_t j = 0; j < N; j++)
            {
                if (op[j])
                    k = j;
            }
            if (k < 0)
                throw "Bad operand";
            for (size_t i = 0; i < bits + 1; i++)
            {
                res[i] = values[k][i];
            }
            return res;
        }

    public:
        Runner() {

        }

        optional<int> simulate(vector<InstrB>& program, vector<int> inputs)
        {
            vector<optional<int>> values(program.size());
            for (size_t i = 0; i < inputs.size(); i++)
            {
                values[i] = inputs[i];
            }

            for (size_t i = inputs.size(); i < program.size(); i++)
            {
                if (program[i].isWAdd)
                {
                    optional<int> OpA = values[program[i].getOpA()];
                    optional<int> OpB = values[program[i].getOpB()];
                    if (OpA.has_value() && OpB.has_value())
                        values[i] = (*OpA + *OpB) & ((1 << bits) - 1);
                }
                else if (program[i].isWSub)
                {
                    optional<int> OpA = values[program[i].getOpA()];
                    optional<int> OpB = values[program[i].getOpB()];
                    if (OpA.has_value() && OpB.has_value())
                        values[i] = (*OpA - *OpB) & ((1 << bits) - 1);
                }
                else if (program[i].isWNeg)
                {
                    optional<int> OpB = values[program[i].getOpB()];
                    if (OpB.has_value())
                        values[i] = (0 - *OpB) & ((1 << bits) - 1);
                }
                else if (program[i].isTAdd)
                {
                    optional<int> OpA = values[program[i].getOpA()];
                    optional<int> OpB = values[program[i].getOpB()];
                    if (OpA.has_value() && OpB.has_value()) {
                        int sum = sign_extend<bits>(*OpA) + sign_extend<bits>(*OpB);
                        if (fits_in<bits>(sum))
                            values[i] = sum & ((1 << bits) - 1);
                    }
                }
                else if (program[i].isTSub)
                {
                    optional<int> OpA = values[program[i].getOpA()];
                    optional<int> OpB = values[program[i].getOpB()];
                    if (OpA.has_value() && OpB.has_value()) {
                        int sum = sign_extend<bits>(*OpA) - sign_extend<bits>(*OpB);
                        if (fits_in<bits>(sum))
                            values[i] = sum & ((1 << bits) - 1);
                    }
                }
                else if (program[i].isTNeg)
                {
                    optional<int> OpB = values[program[i].getOpB()];
                    if (OpB.has_value()) {
                        int neg = 0 - sign_extend<bits>(*OpB);
                        if (fits_in<bits>(neg))
                            values[i] = neg & ((1 << bits) - 1);
                    }
                }
                else if (program[i].isAnd)
                {
                    optional<int> OpA = values[program[i].getOpA()];
                    optional<int> OpB = values[program[i].getOpB()];
                    if (OpA.has_value() && OpB.has_value()) {
                        int band = sign_extend<bits>(*OpA) & sign_extend<bits>(*OpB);
                        values[i] = band & ((1 << bits) - 1);
                    }
                }
                else if (program[i].isXor)
                {
                    optional<int> OpA = values[program[i].getOpA()];
                    optional<int> OpB = values[program[i].getOpB()];
                    if (OpA.has_value() && OpB.has_value()) {
                        int bxor = sign_extend<bits>(*OpA) ^ sign_extend<bits>(*OpB);
                        values[i] = bxor & ((1 << bits) - 1);
                    }
                }
                else if (program[i].isOr)
                {
                    optional<int> OpA = values[program[i].getOpA()];
                    optional<int> OpB = values[program[i].getOpB()];
                    if (OpA.has_value() && OpB.has_value()) {
                        int bor = sign_extend<bits>(*OpA) | sign_extend<bits>(*OpB);
                        values[i] = bor & ((1 << bits) - 1);
                    }
                }
                else if (program[i].isNot)
                {
                    optional<int> OpB = values[program[i].getOpB()];
                    if (OpB.has_value()) {
                        int neg = ~sign_extend<bits>(*OpB);
                        values[i] = neg & ((1 << bits) - 1);
                    }
                }
                else {
                    throw "Not handled";
                }
            }
            return values[program.size() - 1];
        }
    };

    std::string printprogram(vector<InstrB>& prog, size_t inputcount, size_t index)
    {
        if (index < inputcount)
            return std::string("abcdefgh").substr(index, 1);
        if (prog[index].isTAdd)
        {
            return "(" + printprogram(prog, inputcount, prog[index].getOpA()) + " +t " + printprogram(prog, inputcount, prog[index].getOpB()) + ")";
        }
        else if (prog[index].isTSub)
        {
            return "(" + printprogram(prog, inputcount, prog[index].getOpA()) + " -t " + printprogram(prog, inputcount, prog[index].getOpB()) + ")";
        }
        else if (prog[index].isTNeg)
        {
            return "(-t " + printprogram(prog, inputcount, prog[index].getOpB()) + ")";
        }
        else if (prog[index].isWAdd)
        {
            return "(" + printprogram(prog, inputcount, prog[index].getOpA()) + " + " + printprogram(prog, inputcount, prog[index].getOpB()) + ")";
        }
        else if (prog[index].isWSub)
        {
            return "(" + printprogram(prog, inputcount, prog[index].getOpA()) + " - " + printprogram(prog, inputcount, prog[index].getOpB()) + ")";
        }
        else if (prog[index].isWNeg)
        {
            return "(- " + printprogram(prog, inputcount, prog[index].getOpB()) + ")";
        }
        else if (prog[index].isAnd)
        {
            return "(" + printprogram(prog, inputcount, prog[index].getOpA()) + " & " + printprogram(prog, inputcount, prog[index].getOpB()) + ")";
        }
        else if (prog[index].isXor)
        {
            return "(" + printprogram(prog, inputcount, prog[index].getOpA()) + " ^ " + printprogram(prog, inputcount, prog[index].getOpB()) + ")";
        }
        else if (prog[index].isOr)
        {
            return "(" + printprogram(prog, inputcount, prog[index].getOpA()) + " | " + printprogram(prog, inputcount, prog[index].getOpB()) + ")";
        }
        else if (prog[index].isNot)
        {
            return "(~ " + printprogram(prog, inputcount, prog[index].getOpB()) + ")";
        }
    }

    std::string printprogram(vector<InstrB>& prog, size_t inputcount)
    {
        return printprogram(prog, inputcount, prog.size() - 1);
    }

    class Verifier
    {
        Runner run;

        optional<vector<int>> enumerateInputs(pair<vector<InstrB>, vector<InstrB>>& programs, size_t inputcount, size_t index, vector<int> &inputs)
        {
            if (index == inputcount) {
                auto r0 = run.simulate(programs.first, inputs);
                auto r1 = run.simulate(programs.second, inputs);
                if (r0.has_value() != r1.has_value() || (r0.has_value() && r1.has_value() && *r0 != *r1)) {
                    //std::cout << "wrong: " << printprogram(programs.first, inputcount) << " == " <<
                    //    printprogram(programs.second, inputcount) << "\n";
                    return { inputs };
                }
                return {};
            }
            else {
                int mask = (1 << bits) - 1;
                for (int i = 0; i < (1 << bits); i++)
                {
                    // iterate in semi-randomized order (seem to work slightly better?)
                    int x = (i + 0xAA) * 13 & mask;
                    inputs[index] = x;
                    auto res = enumerateInputs(programs, inputcount, index + 1, inputs);
                    if (res)
                        return res;
                }
                return {};
            }
        }

    public:
        optional<vector<int>> verify(pair<vector<InstrB>, vector<InstrB>>& programs, size_t inputcount)
        {
            vector<int> inputs(inputcount);
            return enumerateInputs(programs, inputcount, 0, inputs);
        }
    };
}


int main(int argc, char* argv[])
{
    using namespace ArithmeticIdentityMiner;

    Verifier v;
    auto start_time = std::chrono::steady_clock::now();

    size_t inputcount = 2;
    size_t lhs_size = 3;
    size_t rhs_size = 1;

    if (argc == 4) {
        inputcount = std::strtol(argv[1], nullptr, 10);
        if (inputcount < 1 || inputcount > 6) {
            std::cout << "inputcount out of range [1 .. 6]\n";
            return -1;
        }

        lhs_size = std::strtol(argv[2], nullptr, 10);
        if (lhs_size < 0 || lhs_size + inputcount > 16) {
            std::cout << "lhs_size out of range [0 .. 16 - inputcount] \n";
            return -1;
        }

        rhs_size = std::strtol(argv[3], nullptr, 10);
        if (rhs_size < 0 || rhs_size + inputcount > 16) {
            std::cout << "rhs_size out of range [0 .. 16 - inputcount]\n";
            return -1;
        }
    }

    vector<pair<vector<InstrB>, vector<InstrB>>> bannedPrograms;
    do {
        vector<vector<int>> inputs;

        pair<vector<InstrB>, vector<InstrB>> ThePrograms;
        
        while (true)
        {
            // synthesizer must be recreated from scratch every time,
            // otherwise some assert in Glucose is triggered.
            // todo: figure out a way to re-use good learnt clauses across runs
            Synthesizer s{ lhs_size, rhs_size, inputcount, inputs, bannedPrograms };
            auto programs = s.synthesize();
            if (!programs)
            {
                auto end_time = std::chrono::steady_clock::now();
                std::cout << "// Done in " << std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time) <<
                    ". Used a set of " << inputs.size() << " inputs." "\n";
                return 0;
            }			
            auto counterExample = v.verify(*programs, inputcount);
            // if the programs don't actually match, add more inputs to constrain the synthesizer
            if (counterExample) {
                inputs.push_back(*counterExample);
                continue;
            }
            else {
                ThePrograms = *programs;
                break;
            }
        }

        bannedPrograms.push_back(ThePrograms);

        bool areEqual = false;
        if (ThePrograms.first.size() == ThePrograms.second.size())
        {
            areEqual = true;
            for (size_t i = inputcount; i < ThePrograms.first.size(); i++)
            {
                if (ThePrograms.first[i] != ThePrograms.second[i])
                    areEqual = false;
            }
        }

        if (!areEqual) {
            // print equivalent programs, use endl to flush to ensure saving progress when outputting to a file
            std::cout << printprogram(ThePrograms.first, inputcount) << " == " <<
                printprogram(ThePrograms.second, inputcount) << std::endl;
        }
    } while (1);

}